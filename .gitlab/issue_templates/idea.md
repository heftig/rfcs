<!--
This template should be used for proposing a new RFC idea, discussing it and
potentially finding collaborators to work on a merge request (the eventual
RFC).
-->

# Idea:
<!--
Add the title after the "Idea: "
-->

## Summary
<!--
Add a short summary.
-->

## Specification
<!--
If there is a more defined specification already, add it here.
-->

## Drawbacks
<!--
If there are any known drawbacks, add them here.
-->


<!--
NOTE: Do not remove the below text.
-->
/label ~idea ~help wanted
